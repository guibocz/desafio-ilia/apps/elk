resource "kubernetes_namespace" "elk-namespace" {
  metadata {
    name = "elk-namespace"
  }

  lifecycle {
    ignore_changes = [
      metadata[0].annotations,
      metadata[0].labels,
    ]
  }
}
