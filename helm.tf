resource "helm_release" "elasticsearch-helm" {
  name       = "elasticsearch-helm"
  repository = "https://helm.elastic.co"
  chart      = "elasticsearch"
  version    = var.elasticsearch_version
  namespace  = kubernetes_namespace.elk-namespace.metadata.0.name

  values = [
    file("values-elasticsearch.yaml")
  ]

  depends_on = [
    kubernetes_namespace.elk-namespace,
  ]
}

resource "helm_release" "kibana-helm" {
  name       = "kibana-helm"
  repository = "https://helm.elastic.co"
  chart      = "kibana"
  version    = var.kibana_version
  namespace  = kubernetes_namespace.elk-namespace.metadata.0.name

  values = [
    file("values-kibana.yaml")
  ]

  depends_on = [
    kubernetes_namespace.elk-namespace,helm_release.elasticsearch-helm,
  ]
}

resource "helm_release" "logstash-helm" {
  name       = "logstash-helm"
  repository = "https://helm.elastic.co"
  chart      = "logstash"
  version    = var.logstash_version
  namespace  = kubernetes_namespace.elk-namespace.metadata.0.name

  depends_on = [
    kubernetes_namespace.elk-namespace,helm_release.kibana-helm,
  ]
}

// resource "helm_release" "metricbeat-helm" {
//   name       = "metricbeat-helm"
//   repository = "https://helm.elastic.co"
//   chart      = "metricbeat"
//   version    = var.metricbeat_version
//   namespace  = kubernetes_namespace.elk-namespace.metadata.0.name

//   values = [
//     file("values-metricbeat.yaml")
//   ]

//   depends_on = [
//     kubernetes_namespace.elk-namespace,helm_release.logstash-helm,
//   ]
// }