###########
### ELK ###
###########

elasticsearch_version = "7.17.1"

kibana_version = "7.17.1"

logstash_version = "7.17.1"

metricbeat_version = "7.17.1"