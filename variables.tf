###########
### ELK ###
###########

variable "elasticsearch_version" {
  type = string
}

variable "kibana_version" {
  type = string
}

variable "logstash_version" {
  type = string
}

variable "metricbeat_version" {
  type = string
}